#include <iostream>


int main()
{
    // Mixed arithmetic tests

    int x = -2;
    unsigned int y = 7;

    std::cout << "\nMixed arithmetic tests (x and y)\n";
    std::cout << x - y << std::endl;  // 4294967287
    std::cout << x + y << std::endl;  // 5
    std::cout << x * y << std::endl;  // 4294967282

    std::cout << static_cast<int>(x - y) << std::endl;  // -9
    std::cout << static_cast<int>(x + y) << std::endl;  // 5
    std::cout << static_cast<int>(x * y) << std::endl;  // -14

    // Assignment test

    unsigned int z = -9;
    std::cout << "\nAssignment test (z)\n";
    std::cout << z << std::endl;  // 4294967287

    // Casting and Arithmetic tests

    unsigned int w = -10;
    std::cout << "\nCasting and Arithmetic tests (w)\n";
    std::cout << w << std::endl;                      // 4294967286
    std::cout << static_cast<int>(w/2) << std::endl;  // 2147483643
    std::cout << static_cast<int>(w)/2 << std::endl;  // -5

    return 0;
}
