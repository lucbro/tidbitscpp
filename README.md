# TidbitsCPP

Repository containing tidbits of information with examples for C++. I nearly always create a small example program when discovering something new, and I wanted a place to store this information for occasional review.